﻿namespace ClassLibrary1
{
    public static class BoolReturner
    {
        public static bool Get(bool isTrue) => isTrue;
        public static bool GetFalse() => Get(false);
        public static bool X() => !GetFalse();
    }
}
