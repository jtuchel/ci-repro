using System;
using ClassLibrary2;
using Xunit;

namespace TestProject2
{
    public class StringCombinerTests
    {
        [Theory]
        [InlineData("a", "b")]
        [InlineData("c", "d")]
        public void Test1(string a, string b)
        {
            Assert.Equal(a + b, StringCombiner.Combine(a, b));
        }
    }
}
