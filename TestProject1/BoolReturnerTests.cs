using System;
using ClassLibrary1;
using Xunit;

namespace TestProject1
{
    public class BoolReturnerTests
    {
        [Theory]
        [InlineData(true, true)]
        [InlineData(false, false)]
        public void ReturnsPassedBool(bool expected, bool actual)
        {
            Assert.Equal(expected, BoolReturner.Get(actual));
        }

        [Fact]
        public void X()
        {
            Assert.True(BoolReturner.X());
        }
    }
}
