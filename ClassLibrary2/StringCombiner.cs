﻿using System;

namespace ClassLibrary2
{
    public static class StringCombiner
    {
        public static string Combine(string a, string b) => a + b;
    }
}
