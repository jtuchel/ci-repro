dotnet test --collect:"XPlat Code Coverage";

reportgenerator -reports:'**/coverage.cobertura.xml' -targetdir:'CoverageReports' -reporttypes:'Cobertura';

[XML]$report = Get-Content CoverageReports/Cobertura.xml

if ($report.coverage.'line-rate' -lt 1)
{
    Write-Host "The code is not yet fully tested"
}

Read-Host -Prompt 'done'
